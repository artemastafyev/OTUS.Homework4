﻿using System.Linq;
using System.Text;

public static class Converter
{
    public static Resources Resources;

    public static string ToString(SaveState saveState)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine($"{Resources.IntVar}: {saveState.intVar}");
        sb.AppendLine($"{Resources.FloatVar}: {saveState.floatVar}");
        sb.AppendLine($"{Resources.IntArray}: {string.Join("; ", saveState.intArray.Select(x => x.ToString()))}");
        sb.AppendLine($"{Resources.FloatArray}: {string.Join("; ", saveState.floatArray.Select(x => x.ToString()))}");
        sb.AppendLine($"{Resources.ResultInt1}: {saveState.resultInt1}");
        sb.AppendLine($"{Resources.ResultInt2}: {saveState.resultInt2}");
        sb.AppendLine($"{Resources.ResultFloat1}: {saveState.resultFloat1}");
        sb.AppendLine($"{Resources.ResultFloat2}: {saveState.resultFloat2}");
        string result = sb.ToString();
        return result;
    }
}

