﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SerializeFileSaver : ISave
{
    public void Save(SaveState saveState)
    {
        var currentDir = Directory.GetCurrentDirectory();

        var path = Path.Combine(currentDir, "Saves/Save.dat");

        BinaryFormatter binaryFormatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            binaryFormatter.Serialize(stream, saveState);
        }
    }
}

