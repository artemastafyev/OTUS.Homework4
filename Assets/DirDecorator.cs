﻿using System.IO;

public class DirDecorator : ISave
{
    public ISave _saveStrategy;

    public DirDecorator(ISave saveStrategy)
    {
        _saveStrategy = saveStrategy;
    }

    public void Save(SaveState saveState)
    {
        if (Directory.Exists("Saves") == false)
            Directory.CreateDirectory("Saves");

        _saveStrategy.Save(saveState);
    }
}