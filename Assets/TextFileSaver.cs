﻿using System.IO;

public class TextFileSaver : ISave
{
    public void Save(SaveState saveState)
    {
        string stringResult = Converter.ToString(saveState);
        var currentDir = Directory.GetCurrentDirectory();
        File.WriteAllText(Path.Combine(currentDir, "Saves/Save.txt"), stringResult);
    }
}
