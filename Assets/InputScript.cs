﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;

// 1 Создать переменную булл, в зависимости от ее значения создать массив int\float +
// 2 Создать переменную типа int\float. Заполнить массив так, чтобы каждый елемент был квадратом предыдущего. +
// 3 Создать и отловить исключение (переполнение значения) +
// 4 Создать функцию на вход передать переменную (которая в задании 2). После выполнения функции.Сделать пункт 3\4
// 5 Создать функцию на вход передать переменную REF(которая в задании 2). После выполнения функции.Сделать пункт 3\4
// 6 Создать функцию на вход передать переменную OUT(которая в задании 2). После выполнения функции.Сделать пункт 3\4
// 7 Объявить структуру, которая будет содержать все элементы для предыдущих заданий.
// 8 Записать ее в файл как в первом занятии.
// 9 Записать ее в файл через сериализацию.
// 10 Считать данные из файла.
public class InputScript : MonoBehaviour
{
    public TMP_InputField BoolField;
    public Resources Resources;

    private bool _flag;
    private int[] _intArray;
    private float[] _floatArray;
    private int _arrayLenght = 10;

    public void Start()
    {
        Converter.Resources = Resources;
    }

    [ContextMenu("Запуск")]
    public void Run()
    {
        int intVar = 2;
        float floatVar = 1.1f;

        _intArray = new int[0];
        _floatArray = new float[0];

        bool.TryParse(BoolField.text, out _flag);

        // 1
        if (_flag)
        {
            _intArray = new int[_arrayLenght];

            // 2
            for (int i = 0; i < _intArray.Length; i++)
            {
                _intArray[i] = intVar;
                intVar *= intVar;
            }
        }
        else
        {
            _floatArray = new float[_arrayLenght];

            // 2
            for (int i = 0; i < _floatArray.Length; i++)
            {
                _floatArray[i] = floatVar;
                floatVar *= floatVar;
            }
        }

        // 3
        try
        {
            checked
            {
                int a = 1;
                a += int.MaxValue;
            }
        }
        catch (OverflowException ex)
        {
            Debug.LogError(ex.Message);
        }

        intVar = 2;
        int resultInt1 = 0, resultInt2 = 0;
        if (_flag)
        {
            // 4
            resultInt1 = EasingSquared(intVar);
            Debug.Log(resultInt1);

            // 5
            EasingSquared(intVar, out resultInt2);
            Debug.Log(resultInt2);

            // 6
            EasingSquared(ref intVar);
            Debug.Log(intVar);
        }

        floatVar = 1.1f;
        float resultFloat1 = 0, resultFloat2 = 0;
        if (_flag == false)
        {
            // 4
            resultFloat1 = EasingSquared(floatVar);
            Debug.Log(resultFloat1);

            // 5
            EasingSquared(floatVar, out resultFloat2);
            Debug.Log(resultFloat2);

            // 6
            EasingSquared(ref floatVar);
            Debug.Log(floatVar);
        }

        // 7
        var saveState = new SaveState();
        saveState.intVar = intVar;
        saveState.floatVar = floatVar;
        saveState.intArray = _intArray;
        saveState.floatArray = _floatArray;
        saveState.resultInt1 = resultInt1;
        saveState.resultInt2 = resultInt2;
        saveState.resultFloat1 = resultFloat1;
        saveState.resultFloat2 = resultFloat2;

        // 8
        ISave saveStrategy = new DirDecorator(new TextFileSaver());
        saveStrategy.Save(saveState);

        // 9
        saveStrategy = new DirDecorator(new SerializeFileSaver());
        saveStrategy.Save(saveState);

        // 10
        var loadedSaveState = Load();
        var loadedText = Converter.ToString(loadedSaveState);
        Debug.Log(loadedText);
    }

    private static SaveState Load()
    {
        var currentDir = Directory.GetCurrentDirectory();

        var path = Path.Combine(currentDir, "Saves/Save.dat");

        BinaryFormatter binaryFormatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(path, FileMode.Open))
        {
            return (SaveState)binaryFormatter.Deserialize(stream);
        }
    }

    private int EasingSquared(int x) => x * x;
    private void EasingSquared(int x, out int result) => result = x * x;
    private void EasingSquared(ref int x) => x = x * x;

    private float EasingSquared(float x) => x * x;
    private void EasingSquared(float x, out float result) => result = x * x;
    private void EasingSquared(ref float x) => x = x * x;
}
