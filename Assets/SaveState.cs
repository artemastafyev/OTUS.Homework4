﻿using System;

[Serializable]
public struct SaveState
{
    public int intVar;
    public float floatVar;
    public int[] intArray;
    public float[] floatArray;
    public int resultInt1;
    public int resultInt2;
    public float resultFloat1;
    public float resultFloat2;
}
