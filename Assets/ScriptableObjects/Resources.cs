using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/Resources", fileName = "New Resources")]
public class Resources : ScriptableObject
{
    [SerializeField]
    private string intVar = "intVar";
    public string IntVar => intVar;

    [SerializeField]
    private string floatVar = "intVar";
    public string FloatVar => intVar;

    [SerializeField]
    private string intArray = "intArray";
    public string IntArray => intArray;

    [SerializeField]
    private string floatArray = "floatArray";
    public string FloatArray => floatArray;

    [SerializeField]
    private string resultInt1 = "resultInt1";
    public string ResultInt1 => resultInt1;

    [SerializeField]
    private string resultInt2 = "resultInt2";
    public string ResultInt2 => resultInt2;

    [SerializeField]
    private string resultFloat1 = "resultFloat1";
    public string ResultFloat1 => resultFloat1;

    [SerializeField]
    private string resultFloat2 = "resultFloat2";
    public string ResultFloat2 => resultFloat2;
}
